﻿using System;
using System.Collections.Generic;
using API_Cambodia.Repository;
using Contracts;
using Microsoft.AspNetCore.Mvc;

namespace API_Cambodia.Controllers
{   
    [Route("api/province")]
    [ApiController]
    public class ProvincesController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public ProvincesController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetAllProvinces()
        {
            try
            {
                var provinces = _repository.Province.GetAllProvinces();
 
                _logger.LogInfo($"Returned all owners from database.");
 
                return Ok(provinces);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        [HttpGet("{id}")]
        public IActionResult GetProvinceById(int id)
        {
            try
            {
                var province = _repository.Province.GetProvinceById(id);
                   
                if (province.Equals(null))
                {
                    _logger.LogError($"Province with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned Province with id: {id}");
                    return Ok(province);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetProvinceById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}