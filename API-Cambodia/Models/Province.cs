﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API_Cambodia.Models
{
    [Table("province")]
    public class Province
    {
        [Key] 
        [Column("procode")]
        public int Id { get; set; }
        
        [Column("prefix")]
        public int Prefix { get; set; }
        
        [Column ("province_kh")]
        public string ProvinceNameKhmer { get; set; } 
            
        [Column ("province")]
        public string ProvinceName { get; set; } 
        
        
    }
}