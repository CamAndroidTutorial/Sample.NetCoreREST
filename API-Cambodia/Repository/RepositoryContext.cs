﻿using API_Cambodia.Models;
using Microsoft.EntityFrameworkCore;

namespace API_Cambodia.Repository
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<Province> Provinces { get; set; }
    }
}