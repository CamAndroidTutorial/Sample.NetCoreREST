﻿using System.Collections.Generic;
using API_Cambodia.Models;
using Contracts;

namespace API_Cambodia.Repository
{
    public interface IProvinceRespository : IRepositoryBase<Province>
    {
        IEnumerable<Province> GetAllProvinces();
        Province GetProvinceById(int Id);
        
    }
}