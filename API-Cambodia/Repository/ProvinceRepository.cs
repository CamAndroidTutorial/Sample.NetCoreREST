﻿using System.Collections.Generic;
using System.Linq;
using API_Cambodia.Models;

namespace API_Cambodia.Repository
{
    public class ProvinceRepository : RepositoryBase<Province>, IProvinceRespository
    {
        public ProvinceRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public IEnumerable<Province> GetAllProvinces()
        {
            return FindAll()
                .OrderBy(province => province.Id);
        }

        public Province GetProvinceById(int Id)
        {
 
            return FindByCondition(province => province.Id.Equals(Id))
                .DefaultIfEmpty(new Province())
                .FirstOrDefault();
        }
    }
}