﻿namespace API_Cambodia.Repository
{
    public interface IRepositoryWrapper
    {
        IProvinceRespository Province { get; }
    }
}