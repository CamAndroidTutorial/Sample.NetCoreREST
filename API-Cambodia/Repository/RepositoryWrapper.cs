﻿namespace API_Cambodia.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {

        private RepositoryContext _repositoryContext;
        private IProvinceRespository _province;

        public IProvinceRespository Province
        {
            get
            {
                if (_province == null)
                {
                    _province = new ProvinceRepository(_repositoryContext);
                }

                return _province;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }
    }
}